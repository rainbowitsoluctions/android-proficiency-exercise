package com.androidproficiencyexercise.data;


import android.content.Context;
import android.support.annotation.NonNull;

import com.androidproficiencyexercise.data.localdata.SharePrefHelper;
import com.androidproficiencyexercise.data.network.AppApiHelper;
import com.androidproficiencyexercise.data.network.models.Fact;

import io.reactivex.Single;

/**
 * Created by Lalit Boraste on 10/10/18.
 */
public class AppDataManger {

    private static AppDataManger mDefaultInstance;
    private SharePrefHelper mSharePrefHelper;
    private AppApiHelper mAppApiHelper;

    private AppDataManger(Context context) {
        mSharePrefHelper = new SharePrefHelper(context);
        mAppApiHelper = new AppApiHelper();
    }

    public static AppDataManger getInstance(Context context) {
        if (mDefaultInstance == null) {
            synchronized (AppDataManger.class) {
                if (mDefaultInstance == null) {
                    mDefaultInstance = new AppDataManger(context);
                }
            }
        }
        return mDefaultInstance;
    }

    @NonNull
    public Single<Fact> getFacts() {
        return mAppApiHelper.getFacts();
    }

    @NonNull
    public String getLocalFactsData() {
        return mSharePrefHelper.getFactsData();
    }


    public void setLocalFactData(@NonNull String data) {
        mSharePrefHelper.setFactData(data);
    }
}
