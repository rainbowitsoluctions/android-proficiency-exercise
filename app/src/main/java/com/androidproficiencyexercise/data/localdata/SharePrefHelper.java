package com.androidproficiencyexercise.data.localdata;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lalit Boraste on 10/10/18.
 */
public class SharePrefHelper {
    private static final String PREF_KEY_FACTS_DATA = "PREF_KEY_FACTS_DATA";
    private static final String SHARE_PREF_NAME = "FACTS";
    private final SharedPreferences mPrefs;

    public SharePrefHelper(Context context) {
        mPrefs = context.getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
    }

    public String getFactsData() {
        return mPrefs.getString(PREF_KEY_FACTS_DATA, null);
    }


    public void setFactData(String data) {
        mPrefs.edit().putString(PREF_KEY_FACTS_DATA, data).apply();
    }
}
