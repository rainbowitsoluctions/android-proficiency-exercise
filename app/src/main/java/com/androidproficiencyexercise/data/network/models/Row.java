package com.androidproficiencyexercise.data.network.models;


import android.os.Parcel;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class Row {
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("imageHref")
    private String imageHref;

    protected Row(Parcel in) {
        title = in.readString();
        description = in.readString();
        imageHref = in.readString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Row row = (Row) o;
        return TextUtils.equals(title, row.title) &&
                TextUtils.equals(description, row.description) &&
                TextUtils.equals(imageHref, row.imageHref);
    }

    @Override
    public int hashCode() {
        return title.hashCode() + description.hashCode() + imageHref.hashCode();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }
}
