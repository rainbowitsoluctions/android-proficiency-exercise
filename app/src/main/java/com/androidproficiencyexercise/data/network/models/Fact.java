package com.androidproficiencyexercise.data.network.models;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class Fact {

    @SerializedName("title")
    private String title;
    @SerializedName("rows")
    private List<Row> rows = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}
