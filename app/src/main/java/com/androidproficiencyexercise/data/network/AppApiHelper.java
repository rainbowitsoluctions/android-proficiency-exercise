package com.androidproficiencyexercise.data.network;


import com.androidproficiencyexercise.data.network.models.Fact;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class AppApiHelper {

    /**
     * Returns the Retrofit client.
     *
     * @return Retrofit
     */
    private Retrofit getClient() {
        return new Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Returns the remote Fact.
     *
     * @return Fact.
     */
    public Single<Fact> getFacts() {
        return getClient().create(RestApi.class).getFacts();
    }
}
