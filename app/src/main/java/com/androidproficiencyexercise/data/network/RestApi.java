package com.androidproficiencyexercise.data.network;


import com.androidproficiencyexercise.data.network.models.Fact;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by Lalit Boraste on 22/9/18.
 * Retrofit CURD operations.
 */
public interface RestApi {
    @GET("/s/2iodh4vg0eortkl/facts.json")
    Single<Fact> getFacts();
}
