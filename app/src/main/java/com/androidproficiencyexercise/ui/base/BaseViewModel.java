package com.androidproficiencyexercise.ui.base;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class BaseViewModel extends Observable implements LifecycleObserver {
    // To hold the Rx Disposable, so they can be cleared once the activity or fragment are destroyed to avoid leakage.
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy() {
        mCompositeDisposable.clear();
    }
}
