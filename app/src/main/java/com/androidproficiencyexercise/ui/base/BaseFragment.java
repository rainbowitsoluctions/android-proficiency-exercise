package com.androidproficiencyexercise.ui.base;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public abstract class BaseFragment extends Fragment implements LifecycleRegistryOwner {
    // To hold Rx disposable.
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    // To bind the fragment life cycle with view model.
    private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);

    @Override
    public void onDestroy() {
        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
        super.onDestroy();
    }

    @NonNull
    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycleRegistry;
    }
}
