package com.androidproficiencyexercise.ui.rows;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.androidproficiencyexercise.data.network.models.Row;
import com.androidproficiencyexercise.utils.GlideUtils;

/**
 * Created by Lalit Boraste on 22/9/18.
 * View model to bind the Row data with layout.
 */
public class RowItemViewModel extends BaseObservable {
    private static Context mContext;
    private Row mRow;

    public RowItemViewModel(Row row, Context context) {
        mRow = row;
        mContext = context;
    }

    @BindingAdapter("imagePath")
    public static void setPath(ImageView view, String path) {
        if (!TextUtils.isEmpty(path)) {
            GlideUtils.loadImage(mContext, path, view);
        }
    }

    /**
     * Binds the image path with the image view
     *
     * @return image url path
     */
    @Bindable
    public String getPath() {
        return mRow.getImageHref();
    }

    /**
     * Binds the title with TextView.
     *
     * @return title
     */
    @Bindable
    public String getTitle() {
        return mRow.getTitle();
    }

    /**
     * Binds the description with TextView.
     *
     * @return description
     */
    @Bindable
    public String getDescription() {
        return mRow.getDescription();
    }

    /**
     * Updates the existing Row with new Row.
     *
     * @param row
     */
    public void setRow(Row row) {
        mRow = row;
    }
}
