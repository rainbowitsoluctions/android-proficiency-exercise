package com.androidproficiencyexercise.ui.rows;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidproficiencyexercise.R;
import com.androidproficiencyexercise.data.network.models.Row;
import com.androidproficiencyexercise.databinding.RowListFragmentBinding;
import com.androidproficiencyexercise.ui.base.BaseFragment;

import java.util.List;

/**
 * Created by Lalit Boraste on 22/9/18.
 * Fragment displays list of Row.
 */
public class RowListFragment extends BaseFragment {

    private RowListFragmentBinding mRowListFragmentBinding;

    public static RowListFragment getInstance() {
        return new RowListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRowListFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.row_list_fragment, container, false);
        RowViewModel rowViewModel = new RowViewModel();
        mRowListFragmentBinding.setRowViewModel(rowViewModel);
        getLifecycle().addObserver(rowViewModel);
        return mRowListFragmentBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setFactsListView();
    }


    /**
     * Sets the recycler view adapter and layout manager.
     */
    private void setFactsListView() {
        RecyclerView recyclerView = mRowListFragmentBinding.recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RowAdapter rowAdapter = new RowAdapter(mCompositeDisposable);
        recyclerView.setAdapter(rowAdapter);
        recyclerView.setHasFixedSize(true);

    }

    /**
     * Update the adapter for the Rows.
     *
     * @param rowList Rows
     */
    public void setRows(@NonNull List<Row> rowList) {
        RowAdapter rowAdapter = (RowAdapter) mRowListFragmentBinding.recyclerView.getAdapter();
        rowAdapter.setFactList(rowList);
    }
}
