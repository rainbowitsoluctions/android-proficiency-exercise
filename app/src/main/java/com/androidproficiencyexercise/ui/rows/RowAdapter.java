package com.androidproficiencyexercise.ui.rows;


import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.androidproficiencyexercise.R;
import com.androidproficiencyexercise.data.network.models.Row;
import com.androidproficiencyexercise.databinding.RowListItemBinding;
import com.androidproficiencyexercise.utils.diffutils.RowListDiffUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Lalit Boraste on 22/9/18.
 * Adapter to show the row content like title, description and image.
 */
public class RowAdapter extends RecyclerView.Adapter<RowAdapter.RowListViewHolder> {

    private List<Row> mRowList;
    private CompositeDisposable mCompositeDisposable;

    public RowAdapter(@NonNull CompositeDisposable compositeDisposable) {
        mCompositeDisposable = compositeDisposable;
        this.mRowList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RowListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowListItemBinding rowListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.row_list_item
                , viewGroup, false);
        return new RowListViewHolder(rowListItemBinding);
    }

    @Override
    public long getItemId(int position) {
        return mRowList.get(position).getTitle().hashCode();
    }

    @Override
    public void onBindViewHolder(@NonNull RowListViewHolder productListViewHolder, int position) {
        productListViewHolder.bindProduct(mRowList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mRowList.size();
    }

    public void setFactList(@NonNull List<Row> rowList) {
        if (mRowList.size() != rowList.size()) {
            clearAndNotify(rowList);
        } else {
            performDifferenceCheck(rowList)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<DiffUtil.DiffResult>() {
                @Override
                public void onSubscribe(Disposable d) {
                    mCompositeDisposable.add(d);
                }

                @Override
                public void onNext(DiffUtil.DiffResult diffResult) {
                    diffResult.dispatchUpdatesTo(RowAdapter.this);
                    // clears the old list and update with the new data.
                    mRowList.clear();
                    mRowList.addAll(rowList);
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }
    }

    private void clearAndNotify(@NonNull List<Row> rowList) {
        mRowList.clear();
        mRowList.addAll(rowList);
        notifyDataSetChanged();
    }

    /**
     * Performs the delta check on separate thread.
     *
     * @param rowList list of Row
     * @return the delta check of old and new row list
     */
    private Observable<DiffUtil.DiffResult> performDifferenceCheck(final List<Row> rowList) {
        return Observable.fromCallable(() -> DiffUtil.calculateDiff(new RowListDiffUtils(mRowList, rowList), true));
    }

    static class RowListViewHolder extends RecyclerView.ViewHolder {
        RowListItemBinding mListItemBinding;

        RowListViewHolder(@NonNull RowListItemBinding listItemBinding) {
            super(listItemBinding.getRoot());
            mListItemBinding = listItemBinding;
        }

        void bindProduct(@NonNull Row row) {
            // Sets the RowItemViewModel
            if (mListItemBinding.getRowItemViewModel() == null) {
                mListItemBinding.setRowItemViewModel(new RowItemViewModel(row, mListItemBinding.getRoot().getContext()));
            } else {
                // RowItemViewModel is already set, so just update the Row.
                mListItemBinding.getRowItemViewModel().setRow(row);
            }
            mListItemBinding.executePendingBindings();
        }

    }
}

