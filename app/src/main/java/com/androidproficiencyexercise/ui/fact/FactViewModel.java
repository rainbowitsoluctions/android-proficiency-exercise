package com.androidproficiencyexercise.ui.fact;


import android.databinding.ObservableInt;
import android.view.View;

import com.androidproficiencyexercise.R;
import com.androidproficiencyexercise.data.AppDataManger;
import com.androidproficiencyexercise.data.network.models.Fact;
import com.androidproficiencyexercise.ui.base.BaseViewModel;
import com.androidproficiencyexercise.utils.Utils;
import com.google.gson.Gson;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class FactViewModel extends BaseViewModel {
    public ObservableInt loadingProgress;

    public FactViewModel() {
        loadingProgress = new ObservableInt(View.GONE);
    }

    private void notifyListUpdated(Fact fact) {
        setChanged();
        notifyObservers(fact);
    }

    public void getFacts(FactMainActivity factMainActivity,boolean showLoader) {
        if (Utils.isOnline(factMainActivity)) {
            AppDataManger.getInstance(factMainActivity).getFacts()
                    .subscribeOn(Schedulers.io())
                    .map(fact -> {
                        if (fact != null) {
                            Gson gson = new Gson();
                            String factsToString = gson.toJson(fact);
                            AppDataManger.getInstance(factMainActivity).setLocalFactData(factsToString);
                        }
                        return fact;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Fact>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mCompositeDisposable.add(d);
                            if (showLoader) {
                                loadingProgress.set(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onSuccess(Fact fact) {
                            if (fact != null) {
                                notifyListUpdated(fact);
                            }
                            loadingProgress.set(View.GONE);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            loadingProgress.set(View.GONE);
                        }
                    });
        } else {
            factMainActivity.showToast(factMainActivity.getString(R.string.offline));
            loadingProgress.set(View.VISIBLE);
            String factsString = AppDataManger.getInstance(factMainActivity).getLocalFactsData();
            Gson gson = new Gson();
            Fact fact = gson.fromJson(factsString, Fact.class);
            if (fact != null) {
                notifyListUpdated(fact);
            }
            loadingProgress.set(View.GONE);
        }
    }
}
