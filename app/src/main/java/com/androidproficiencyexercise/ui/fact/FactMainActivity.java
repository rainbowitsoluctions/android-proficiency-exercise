package com.androidproficiencyexercise.ui.fact;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.androidproficiencyexercise.R;
import com.androidproficiencyexercise.data.network.models.Fact;
import com.androidproficiencyexercise.databinding.ActivityFactMainBinding;
import com.androidproficiencyexercise.ui.base.BaseActivity;
import com.androidproficiencyexercise.ui.rows.RowListFragment;

import java.util.Observable;

/**
 * Created by Lalit Boraste on 22/9/18.
 * Activity which loads the @RowListFragment, pulls the Facts contents and pass on to @RowListFragment.
 */
public class FactMainActivity extends BaseActivity {

    private ActivityFactMainBinding mActivityFactsMainBinding;
    private boolean mShowLoader = true;

    private void loadFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityFactsMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_fact_main);
        loadFragment(RowListFragment.getInstance());
        FactViewModel factViewModel = new FactViewModel();
        factViewModel.addObserver(this);
        // To bind activity life cycle events with View Model.
        getLifecycle().addObserver(factViewModel);
        mActivityFactsMainBinding.setFactViewModel(factViewModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FactViewModel factViewModel = mActivityFactsMainBinding.getFactViewModel();
        if (factViewModel != null) {
            factViewModel.getFacts(this, mShowLoader);
        }
        mShowLoader = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.facts_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                FactViewModel factViewModel = mActivityFactsMainBinding.getFactViewModel();
                if (factViewModel != null) {
                    factViewModel.getFacts(this,!mShowLoader);
                }
                break;
        }
        return true;
    }

    /**
     * Observes for the Facts data from the @FactViewModel
     *
     * @param observable
     * @param arg
     */
    @Override
    public void update(Observable observable, Object arg) {
        if (arg != null) {
            try {
                Fact fact = (Fact) arg;
                setActionBarTitle(fact.getTitle());
                RowListFragment rowListFragment = (RowListFragment) getSupportFragmentManager().findFragmentById(R.id.container);
                if (rowListFragment != null) {
                    rowListFragment.setRows(fact.getRows());
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        } else {
            showToast(getString(R.string.went_wrong));
        }
    }
}
