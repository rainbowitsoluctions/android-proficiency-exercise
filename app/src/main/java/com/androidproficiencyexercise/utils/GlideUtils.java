package com.androidproficiencyexercise.utils;


import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.androidproficiencyexercise.R;
import com.bumptech.glide.Glide;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class GlideUtils {

    /**
     * Loads the image with Glide.
     *
     * @param context
     * @param path      url path of image
     * @param imageView holder to load the url image
     */
    public static void loadImage(@NonNull Context context, @NonNull String path, @NonNull ImageView imageView) {
        Glide.with(context)
                .load(path)
                .fitCenter()
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);
    }
}
