package com.androidproficiencyexercise.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

/**
 * Created by Lalit Boraste on 22/9/18.
 */
public class Utils {
    /**
     * Utility method to determine whether the device has network connection or not.
     *
     * @param context Context
     * @return true if connected to internet, else false
     */
    public static boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm == null) {
            return false;
        }
        activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
