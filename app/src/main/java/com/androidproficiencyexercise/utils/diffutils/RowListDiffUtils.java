package com.androidproficiencyexercise.utils.diffutils;


import android.support.v7.util.DiffUtil;
import android.text.TextUtils;

import com.androidproficiencyexercise.data.network.models.Row;

import java.util.List;

/**
 * Created by Lalit Boraste on 22/9/18.
 * Class to avoid un-necessary refresh to adapter, it checks if the data in the old and new lsit are chanage or not.
 * If changed then notifies the adapter for data change.
 */
public class RowListDiffUtils extends DiffUtil.Callback {

    private List<Row> mOldRowList;
    private List<Row> mNewRowList;

    public RowListDiffUtils(List<Row> oldRowList, List<Row> newRowList) {
        this.mOldRowList = oldRowList;
        this.mNewRowList = newRowList;
    }

    @Override
    public int getOldListSize() {
        return mOldRowList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewRowList.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        // as there is no unique id, so considering unique as a title.
        return TextUtils.equals(mOldRowList.get(i).getTitle(), mNewRowList.get(i1).getTitle());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        // if title is same then other parameters will be checked.
        return mNewRowList.get(i).equals(mOldRowList.get(i1));
    }
}
